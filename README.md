# homebridge-xbox-one-ng

Homebridge plugin to turn on and off Xbox One. Inspired by `homebridge-xbox-one`

## Installation

```
npm install -g homebridge-xbox-one-ng
```

## Configuration

Add this to your `~/.homebridge/config.json` as an accessory:
```
{
  "accessory": "homebridge-xbox-one-ng.XboxOne",
  "name": "XboxOne",
  "ipAddress": "<Xbox IP address>",
  "liveId": "<Xbox Live ID>"
}
```

## Getting your Xbox One's IP address

On your Xbox, go to Settings > Network > Network Settings > Advanced Settings

## Getting your Live ID

On your Xbox, go to Settings > System > Console info & updates and look under "Xbox Live device ID"
