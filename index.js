var Smartglass = require('xbox-smartglass-core-node');

var Service, Characteristic;

module.exports = (homebridge) => {
  Service = homebridge.hap.Service;
  Characteristic = homebridge.hap.Characteristic;
  homebridge.registerAccessory("homebridge-xbox-one-ng", "XboxOne", XboxAccessory);
};

class XboxAccessory {
  constructor(log, config) {
    this.log = log
    this.name = config['name'] || 'XboxOne';
    this.log(`Creating '${this.name}'...`);
    this.tries = config['tries'] || 5;
    this.live_id = config['liveId'];
    this.ip = config['ipAddress'];

    setInterval(async () => {
      this.log(`Updating powerstate...`);
      let sgClient = Smartglass();
      try {
        await sgClient.connect(this.ip)
        this.log('Xbox succesfully connected!');
        this.powerState = true;
      } catch (e) {
        this.powerState = false;
      }
      this.log(`Updated powerstate:  ${this.powerState ? "on": "off"}`);
    }, 15000);
  }

  setPowerState = async (powerOn, callback) => {
    this.log(`Sending on command to '${this.name}'...`);

    if (powerOn) {
      this.log(`powering on...`);
      setImmediate(async () => {
        try {
          let response = await Smartglass().powerOn({
            live_id: this.live_id,
            tries: this.tries,
            ip: this.ip,
          })
          this.log('Console booted:', response);
          this.powerState = true;
        } catch (e) {
          this.log('Booting console failed:', e);
        }
      });
    } else {
      this.log(`powering off...`);
      setImmediate(async () => {
        let sgClient = Smartglass();
        try {
          await sgClient.connect(this.ip);
          this.log('Xbox succesfully connected!');
          await sgClient.powerOff();
          this.log('Shutdown succes!');
          this.powerState = false;
        } catch (e) {
          this.log('Shutdown error:', e);
        }
      });
    }

    // Always report success to not hang up homebridge.
    callback(null, powerOn);
  }


  getPowerState = (callback) => {
    this.log(`reporting power state ${this.powerState ? "on": "off"}`);
    callback(null, this.powerState);
  }

  getServices = () => {
    let switchService = new Service.Switch(this.name);

    switchService
      .getCharacteristic(Characteristic.On)
      .on('set', this.setPowerState)
      .on('get', this.getPowerState);

    return [switchService];
  }
}
